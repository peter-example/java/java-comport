package hk.quantr.javacomport;

import com.fazecast.jSerialComm.SerialPort;
import java.io.IOException;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author Peter <peter@quantr.hk>
 */
public class JavaComPort {

	public static void main(String[] args) {
		SerialPort[] serialPorts = SerialPort.getCommPorts();
		for (SerialPort port : serialPorts) {
			if (port.toString().equals("STM32 Virtual ComPort")) {
				try {
					System.out.println(port.getDescriptivePortName());
					if (port.openPort()) {
						System.out.println("send");
						port.getOutputStream().write('a');
						port.getOutputStream().flush();
						port.closePort();
					}
				} catch (IOException ex) {
					Logger.getLogger(JavaComPort.class.getName()).log(Level.SEVERE, null, ex);
				}
			}
		}
	}
}
